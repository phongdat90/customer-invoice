<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;
use App\Models\InvoiceItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Validator;
class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()
            ->json([
                'model' => Invoice::with('customer')->FilterPaginateDataTable($request)
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()
            ->json([
                'form'   => Invoice::initialize(),
                'option' => Customer::orderBy('name')->get()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,id',
            'title'       => 'required',
            'date'        => 'required|date_format:Y-m-d',
            'due_date'    => 'required|date_format:Y-m-d',
            'discount'    => 'required|numeric|min:0',
            'items'       => 'required|array|min:1',
            'items.*.description' => 'required|max:255',
            'items.*.qty'         => 'required|numeric|min:1',
            'items.*.unit_price'  => 'required|numeric|min:0'
        ])->validate();
        $data = $request->except('items');
        $data['sub_total'] = 0;
        $items = [];
        foreach($request->items as $item) {
            $data['sub_total'] += $item['unit_price'] * $item['qty'];
            $items[] = new InvoiceItem($item);
        }
        $data['total'] = $data['sub_total'] - $data['discount'];
        $invoice = Invoice::create($data);
        $invoice->items()
                ->saveMany($items);
        return response()
            ->json([
                'saved' => true,
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::with('customer','items')->findOrFail($id);
        return response()
            ->json([
                    'model' => $invoice
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::with('items')->findOrFail($id);
        return response()
            ->json([
                'form'   => $invoice,
                'option' => Customer::orderBy('name', 'desc')->get()
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'customer_id' => 'required|exists:customers,id',
            'title'       => 'required',
            'date'        => 'required|date_format:Y-m-d',
            'due_date'    => 'required|date_format:Y-m-d',
            'discount'    => 'required|numeric|min:0',
            'items'       => 'required|array|min:1',
            'items.*.description' => 'required|max:255',
            'items.*.qty'         => 'required|numeric|min:1',
            'items.*.unit_price'  => 'required|numeric|min:0'
        ]);
        $invoice = Invoice::findOrFail($id);
        $data = $request->except('items');
        $data['sub_total'] = 0;
        $items = [];
        $itemsIds = [];
        foreach($request->items as $item) {
            $data['sub_total'] += $item['unit_price'] * $item['qty'];
            if(isset($item['id'])) {
                InvoiceItem::whereId($item['id'])
                            ->whereInvoiceId($id)
                            ->update($item);
                $itemsIds[] = $item['id'];
            } else {
                $items[] = new InvoiceItem($item);
            }
        }
        $data['total'] = $data['sub_total'] - $data['discount'];
        $invoice->update($data);
        // delete remove item
        if(count($itemsIds)) {
//            InvoiceItem::whereInvoiceId($invoice->id)
//                        ->whereNotIn('id', $itemsIds)
//                        ->delete();
            InvoiceItem::where('invoice_id',$invoice->id)
                        ->whereNotIn('id', $itemsIds)
                        ->delete();
        }
        //save items
        if(count($items)) {
            $invoice->items()
                    ->saveMany($items);
        }
        return response()
                ->json([
                    'saved' => true
                ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $invoice = Invoice::findOrFail($id);
        $InvoiceItems = $invoice->items()->get();
        foreach($InvoiceItems as $row) {
            $row->delete();
        }
        //InvoiceItem::whereInvoiceId($invoice->id)->delete();
        $invoice->delete();
        return response()
            ->json([
                'delete' => true
            ]);
    }
}
