<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Support\FilterPaginateDataTable;
class Customer extends Model
{
    use FilterPaginateDataTable;
    //
    protected $table = 'customers';
    protected $fillable = ['name', 'company', 'email', 'phone', 'address'];
    protected $filter = ['id', 'name', 'company', 'email', 'phone', 'address', 'created_at'];

    public static function initialize() {
        return [
          'company' => '', 'email' => '', 'name' => '', 'phone' => '', 'address' => ''
        ];
    }
    // relationship: 1 kh co the co nhieu hoa don
    public function invoices() {
        return $this->hasMany('App\Models\Invoice');
    }

}
