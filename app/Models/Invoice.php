<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Support\FilterPaginateDataTable;
class Invoice extends Model
{
    //
    use FilterPaginateDataTable;
    protected $table = 'invoices';
    protected $fillable = ['customer_id', 'date', 'due_date', 'discount', 'sub_total', 'total', 'title'];
    protected $filter = [
        'id', 'customer_id', 'date', 'due_date', 'discount', 'sub_total', 'total',
        //filter relation also.eg: customer
        'customer.id','customer.company','customer.email','customer.phone',
        'customer.name','customer.address','customer.created_at'
    ];

    public static function initialize() {
        return [
            'customer_id' => 'Select',
            'title'       => 'Invoice for ',
            'date'        => date('Y-m-d'),
            'due_date'    => null,
            'discount'    => 0,
            'items'       => [
                InvoiceItem::initialize()
            ]
        ];
    }
    //1 hoa don chi thuoc ve 1 khach hang
    public function customer() {
        return $this->belongsTo('App\Models\Customer');
    }
    // 1 invoice co nhieu invoiceitems
    public function items() {
        return $this->hasMany('App\Models\InvoiceItem');
    }
}
