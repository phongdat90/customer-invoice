<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    //
    protected $table = 'invoice_items';
    protected $fillable = ['qty', 'description', 'unit_price'];
    public static function initialize() {
        return [
            'description' => '',
            'qty'         => 1,
            'unit_price'  => 0
        ];
    }
}
