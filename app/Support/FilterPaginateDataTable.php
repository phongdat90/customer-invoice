<?php
namespace App\Support;
use Validator;
trait FilterPaginateDataTable {
    protected $operators = [
        'equal_to'     => '=',
        'less_than'    => '<',
        'greater_than' => '>'
    ];

    public function scopeFilterPaginateDataTable($query, $request) {
        $v = Validator::make($request->all(), [
           'search_operator' => 'required|in:'.implode(',', array_keys($this->operators)),
           'search_column'   => 'required|in:'.implode(',', $this->filter ),
           'search_query_1'  => 'max:255',
           'per_page'        => 'required|integer|min:1',
           'page'            => 'required|integer|min:1',
           'sort_column'     => 'required|in:'.implode(',', $this->filter),
           'direction'       => 'required|in:asc,desc'

        ]);
        if($v->fails()) {
            dd($v->messages());
        }
        return $query->orderBy($request->sort_column, $request->direction)
            ->where(function($query) use ($request){
                if($request->has('search_query_1')) {
                    // determine the type of search_column
                    // check if its related model, eg: customer.id
                    if($this->isRelatedColumn($request) !== false) {
                        list($relation, $relatedColumn) = explode('.', $request->search_column);
                        return $query->whereHas($relation, function($query) use ($relatedColumn, $request){
                            return $this->builQuery(
                                $relatedColumn,
                                $request->search_operator,
                                $request,
                                $query
                            );
                        });
                    } else {
                        return $this->builQuery(
                            $request->search_column,
                            $request->search_operator,
                            $request,
                            $query
                        );
                    }

                }
            })
            ->paginate($request->per_page);
    }
    protected function builQuery($column, $operator, $request, $query) {
        switch ($operator) {
            case 'equal_to' :
            case 'less_than':
            case 'greater_than':
                $query->where($column, $this->operators[$operator], $request->search_query_1);
                break;
            default :
                break;
        }
        return $query;
    }
    protected function isRelatedColumn($request) {
        return strpos($request->search_column, '.');
    }
}