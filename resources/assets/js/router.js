import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
const router = new VueRouter({
   //mode: 'history', //bo # tren url. che de hash mode
    routes: [
        {path: '/customer', component: require('./customer/index.vue')},
        {path: '/customer/create', component: require('./customer/form.vue')},
        {path:'/customer/:id', component: require('./customer/show.vue')},
        {path: '/customer/:id/edit', component: require('./customer/form.vue'), meta: {mode: 'edit'}},

        {path: '/invoice', component: require('./invoice/index.vue')},
        {path: '/invoice/create', component: require('./invoice/form.vue')},
        {path: '/invoice/:id', component: require('./invoice/show.vue')},
        {path: '/invoice/:id/edit', component: require('./invoice/form.vue'), meta: {mode: 'edit'}},

        {path: '/data-table', component: require('./testDataTable/index.vue')}
    ]

});
export default router